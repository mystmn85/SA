from flask import Flask, redirect, url_for
from app.controller.basic.view import basic_blueprint
import os

# Create flask app
app = Flask(__name__, instance_relative_config=True)
app.config.from_object('config.Dev')

# BLUEPRINTS # [__name__/]__init__.py
app.register_blueprint(basic_blueprint)


@app.route('/')
def index():
    return redirect('/basic/')


@app.route('/t')
def dexing():
    return redirect(url_for('basic.index'))


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
