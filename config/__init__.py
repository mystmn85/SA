import os

basedir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
DB = '/app/DB/'
project_name = "SkinnerAssociates"
path = basedir + project_name + DB


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'v9y$B&E(H+MbQeThWmZq4t7w!z%C*F-J@NcRfUjXn2r5u8x/A?D(G+KbPdSgVkY'


class Prod(Config):
    DEBUG = False
    ENV = 'production'
    SQLALCHEMY_DATABASE_URI = "sqlite:////%s_dev.sqlite" % path


class Dev(Config):
    DEVELOPMENT = True
    DEBUG = True
    ENV = 'development'
    MAIL_DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_URI = "sqlite:////tmp/%s_dev.sqlite" % path


class Test(Config):
    TESTING = True
    ENV = 'testing'
    SQLALCHEMY_DATABASE_URI = "sqlite:////tmp/%s_test.sqlite" % path
