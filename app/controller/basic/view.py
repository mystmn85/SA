from flask import render_template, Blueprint, url_for
from random import choice
from . import colors, title

basic_blueprint = Blueprint('basic', __name__, url_prefix="/basic", template_folder='templates',
                            static_folder="static")

footer_menu = {
    'sitemap': {
        'index': '/',
        'attorneys':  'attorneys',
        'blog':  'blog',
        'practice':  'practice',
        'staff':  'staff',
    }
    #    'title': ['Home', 'Attorneys', 'Blog', 'Practice', 'Staff']
}


@basic_blueprint.route('/')
def index():
    spec = {
        'title': "Index Page {}".format(title),
        'name': 'Home'
    }
    style2 = "background-color:{};".format(choice(colors))
    return render_template('index.html', spec=spec, style2=style2, fm=footer_menu)


@basic_blueprint.route('/attorneys')
def attorneys():
    spec = {
        'title': "Attorneys {}".format(title),
        'name': 'Attorneys'
    }
    return render_template('attorneys.html', spec=spec, fm=footer_menu)


@basic_blueprint.route('/blog')
def blog():
    spec = {
        'title': "Blog Page {}".format(title),
        'name': 'Blog'
    }
    return render_template('blog.html', spec=spec, fm=footer_menu)


@basic_blueprint.route('/practice')
def practice():
    spec = {
        'title': "Practice Page {}".format(title),
        'name': 'Practice'
    }
    return render_template('practice.html', spec=spec, fm=footer_menu)


@basic_blueprint.route('/staff')
def staff():
    spec = {
        'title': "Staff Page {}".format(title),
        'name': 'Staff'
    }
    return render_template('staff.html', spec=spec, fm=footer_menu)


@basic_blueprint.route('/reward')
def reward():
    spec = {
        'title': "Reward Page {}".format(title),
        'name': 'Reward'
    }
    return render_template('reward.html', spec=spec, fm=footer_menu)


basic_blueprint.add_url_rule('/', view_func=index)
basic_blueprint.add_url_rule('/test', view_func=attorneys)
