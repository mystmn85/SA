
title = "| Skinner & Associates"
colors = [
    "#C0C0C0",
    "#808080",
    "#000000",
    "#FF0000",
    "#800000",
    "#FFFF00",
    "#808000",
    "#00FF00",
    "#008000",
    "#00FFFF",
    "#008080",
    "#0000FF",
    "#000080",
    "#FF00FF",
    "#800080",
    "#FF7F50",
    "#FF6347",
    "#FF4500",
    "#FFD700",
    "#FFA500",
    "#FF8C00",
    "#9ACD32",
    "#00FF7F",
    "#00FA9A",
    "#90EE90",
    "#98FB98",
    "#8FBC8F",
    "#3CB371",
    "#2E8B57",
    "#808000",
    "#556B2F",
    "#6B8E23",
    "#B0E0E6",
    "#ADD8E6",
    "#87CEFA",
    "#87CEEB",
    "#00BFFF",
    "#B0C4DE",
    "#1E90FF",
    "#6495ED",
    "#4682B4",
    "#4169E1",
    "#0000FF",
    "#0000CD",
    "#00008B",
    "#000080",
    "#191970",
    "#7B68EE",
    "#6A5ACD",
    "#483D8B",
    "#FFF8DC",
    "#FFEBCD",
    "#FFE4C4",
    "#FFDEAD",
    "#F5DEB3",
    "#DEB887",
    "#D2B48C",
    "#BC8F8F",
    "#F4A460",
    "#DAA520",
    "#CD853F",
    "#D2691E",
    "#8B4513",
    "#A0522D",
    "#A52A2A",
    "#800000",
    "#DCDCDC",
    "#D3D3D3",
    "#C0C0C0",
    "#A9A9A9",
    "#808080",
    "#696969",
    "#778899",
    "#708090",
    "#2F4F4F"
]
